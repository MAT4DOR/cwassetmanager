Asset Manager for Cube World. Decodes/Encodes the blobs from the .db files.

Requires: SQLite JDBC Driver found at https://bitbucket.org/xerial/sqlite-jdbc

Usage:

	encode <path-to-folder> - Encode resources from folder.

	decode <path-to-db> - Decode resource from .db.

By MAT4DOR