package cwtools;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class CWAssetManager {
	
	static int[] table = new int[44];
	static byte[] w = new byte[4];
	
	public static void loadTable() throws IOException {
		DataInputStream in = new DataInputStream(CWAssetManager.class.getResourceAsStream("/table.dat"));
		for(int i=0;i<44;i++)
			table[i] = littleInt(in);
		System.out.println(table[0]);
		in.close();
	}
	
	public static int littleInt(DataInputStream in) throws IOException {
		in.readFully(w, 0, 4);
		return w[3] << 24 | (w[2] & 0xff) << 16 | (w[1] & 0xff) << 8 | (w[0] & 0xff);
	}
	
	public static void swap(byte[] data, int i, int j) {
		byte b = data[i];
		data[i] = data[j];
		data[j] = b;
	}
	
	public static void pass(byte[] data) {
		for(int i=0;i<data.length;i++) {
			data[i] = (byte) (-1 - data[i]);
		}
	}
	
	public static void decode(byte[] data) {
		for(int i = data.length-1; i >= 0; i--) {
			swap(data, i, (i + table[i % 44]) % data.length);
		}
		pass(data);
	}
	
	public static void encode(byte[] data) {
		for(int i = 0; i < data.length; i++) {
			swap(data, i, (i + table[i % 44]) % data.length);
		}
		pass(data);
	}
	
	public static void main(String[] args) throws IOException {
		if(args.length == 2) {
			loadTable();
			if(args[0].equalsIgnoreCase("encode")) {
				File file = new File(args[1]);
				if(file.exists() && file.isDirectory()) {
					String dbFile = file.getAbsolutePath();
					if(dbFile.lastIndexOf("/") == dbFile.length() - 1)
						dbFile = dbFile.substring(0, dbFile.length() - 1);
					dbFile += "_" + new Date().getTime() + ".db";
					
					File db = new File(dbFile);
					if(db.exists())
						db.delete();
					
					Connection conn;
					try {
						Class.forName("org.sqlite.JDBC");
						conn = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
						
						Statement stmt;
						stmt = conn.createStatement();
						String sql = "CREATE TABLE blobs(key TEXT PRIMARY KEY, value BLOB);";
						stmt.execute(sql);
						stmt.close();
						
						sql = "INSERT INTO blobs(key, value) VALUES(?, ?)";
						PreparedStatement pstmt = conn.prepareStatement(sql);
						File[] files = file.listFiles();
						for(File f: files) {
							FileInputStream in = new FileInputStream(f);
							byte[] bytes = new byte[(int) f.length()];
							in.read(bytes);
							in.close();
							encode(bytes);
							pstmt.setString(1, f.getName());
							pstmt.setBytes(2, bytes);
							pstmt.execute();
							System.out.println("Added: " + f.getName());
						}
						pstmt.close();
						conn.close();
						System.out.println("Complete.");
					} catch(Exception e) {
						e.printStackTrace();
					}
					return;
				} else {
					System.out.println("Path does not exist, or is not a directory.\n");
				}
			} else if(args[0].equalsIgnoreCase("decode")) {
				File file = new File(args[1]);
				if(file.exists() && !file.isDirectory()) {
					Connection conn;
					Statement stmt;
					try {
						Class.forName("org.sqlite.JDBC");
						conn = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath());
						
						String dir = file.getParent() + "/" + file.getName().replace(".db", "") + "/";
						File dirFile = new File(dir);
						if(!dirFile.exists())
							dirFile.mkdir();
						
						stmt = conn.createStatement();
						String sql = "SELECT * FROM blobs";
						stmt.execute(sql);
						ResultSet set = stmt.getResultSet();
						while(set.next()) {
							byte[] bytes = set.getBytes("value");
							decode(bytes);
							FileOutputStream out = new FileOutputStream(dir + set.getString("key"));
							System.out.println("Decoding: " + set.getString("key"));
							out.write(bytes, 0,bytes.length);
							out.close();
						}
						stmt.close();
						conn.close();
						System.out.println("Complete.");
					} catch(Exception e) {
						e.printStackTrace();
					}
					return;
				} else {
					System.out.println("File doesn't exists, or is a directory.\n");
				}
			}
		}
		System.out.println("Cube World Asset Manager alpha02 by MAT4DOR.");
		System.out.println("Usage:");
		System.out.println("\tencode <path-to-folder>\t- Encode resources from folder.");
		System.out.println("\tdecode <path-to-db>\t- Decode resource from .db.");
	}
}
